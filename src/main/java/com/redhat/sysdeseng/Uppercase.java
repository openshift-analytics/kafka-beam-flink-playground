/*
 * OpenShift Event Stream Analytics
 *
 * Copyright © 2017 Red Hat, Inc.
 *
 * This file is part of OpenShift Event Stream Analytics.
 *
 * OpenShift Event Stream Analytics is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenShift Event Stream Analytics is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenShift Event Stream Analytics. If not, see <http ://www.gnu.org/licenses/>.
 */

package com.redhat.sysdeseng;

import org.apache.beam.sdk.transforms.DoFn;

/** A {@link DoFn} that uppercases a word. */
class Uppercase extends DoFn<String, String> {
    @ProcessElement
    public void processElement(ProcessContext c) {
        c.output(c.element().toUpperCase());
    }
}