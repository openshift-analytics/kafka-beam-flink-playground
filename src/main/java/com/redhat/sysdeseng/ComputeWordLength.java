/*
 * OpenShift Event Stream Analytics
 *
 * Copyright © 2017 Red Hat, Inc.
 *
 * This file is part of OpenShift Event Stream Analytics.
 *
 * OpenShift Event Stream Analytics is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenShift Event Stream Analytics is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenShift Event Stream Analytics. If not, see <http ://www.gnu.org/licenses/>.
 */

package com.redhat.sysdeseng;

import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.DoFn.ProcessElement;

/** A {@link DoFn} that get the lenght of a word. */
class ComputeWordLength extends DoFn<String, Integer> {
    @ProcessElement
    public void processElement(ProcessContext c) {
        // Get the input element from ProcessContext.
        String word = c.element();
        // Use ProcessContext.output to emit the output element.
        c.output(word.length());
        System.out.println(word);
    }
}