/*
 * OpenShift Event Stream Analytics
 *
 * Copyright © 2017 Red Hat, Inc.
 *
 * This file is part of OpenShift Event Stream Analytics.
 *
 * OpenShift Event Stream Analytics is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenShift Event Stream Analytics is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenShift Event Stream Analytics. If not, see <http ://www.gnu.org/licenses/>.
 */

package com.redhat.sysdeseng;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.apache.beam.sdk.coders.StringUtf8Coder;
import org.apache.beam.sdk.testing.TestPipeline;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollection;

@RunWith(JUnit4.class)
public class ComputeWordLengthTest implements Serializable {
    @Rule
    public TestPipeline p = TestPipeline.create();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    // Our static input data, which will comprise the initial PCollection.
    static final String[] WORDS_ARRAY = new String[] { "there", "hi", "hi sue bob", "hi sue", "bob hi" };

    static final List<String> WORDS = Arrays.asList(WORDS_ARRAY);

    static final Integer[] WORDLENGTH_ARRAY = new Integer[] { 5, 2, 10, 6, 6 };

    // Example test that tests the pipeline's transforms.

    @Test
    public void testUppercase() throws Exception {
        // Create a PCollection from the WORDS static input data.
        PCollection<String> input = p.apply(Create.of(WORDS)).setCoder(StringUtf8Coder.of());

        // Run ALL the pipeline's transforms ...
        PCollection<Integer> output = input.apply(ParDo.of(new ComputeWordLength()));

        // FITME Assertion is missing...

        // Run the pipeline.
        p.run();
    }
}